package jade;
import java.util.ArrayList;
import java.util.List;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.AMSService;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


public class EMA extends Agent
{
	//send START message to all SA 
	// start timer 
	//stop timer after processing all msgs by all MCA
    public static int M = 30;
    public static int N = 20;
    static ArrayList<SA> agents = new ArrayList<SA>();
    public static int numOfMachines = 2;
    public static AID EMAID = null;
    private int counter = 0;
    private long start = 0;
    
    protected void setup()
	{
		addBehaviour(new OneShotBehaviour(this)
		{
			public void action()
			{
				EMAID=this.myAgent.getAID();
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setContent("START");
				while(agents.size()!=numOfMachines)
				{}
				for (int i=0; i<agents.size();i++)
				{
				     AID agentID = agents.get(i).getAID();
				     //System.out.println("Start sent to " + agentID.getLocalName());
				     msg.addReceiver(agentID);
				}
				myAgent.send(msg);
				start = System.currentTimeMillis();   
			}
		});
		
		addBehaviour(new CyclicBehaviour(this)
		{
			public void action()
			{
				while(counter!=numOfMachines)
				{
					MessageTemplate mti = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
					ACLMessage rcvmsg = myAgent.receive(mti);
					if (rcvmsg!=null && rcvmsg.getContent().equals("DONE"))
					{
						//System.out.println(myAgent.getLocalName()+ " received msg: " + rcvmsg.getContent());
						counter++;
					}
					else
						block();
				}
				myAgent.removeBehaviour(this);
				long elapsedTime = System.currentTimeMillis() - start;
				System.out.println(elapsedTime + " ms");
				doDelete();
			}
		}
		);
	}
	
	public static void registerSA(SA agent)
	{
        agents.add(agent);
	}
}
