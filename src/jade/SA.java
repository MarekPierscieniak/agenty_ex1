package jade;
import java.util.ArrayList;
import java.util.Arrays;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


public class SA extends Agent{
	//receive START msg
	//send N message of size M to all MCA's
	  static ArrayList<MCA> MCAagents = new ArrayList<MCA>();
	
	public void setup() {	
		//System.out.println(this.getLocalName() + "register");
		EMA.registerSA(this);
		addBehaviour(new CyclicBehaviour(this)
		{
			public void action()
			{
				while(MCAagents.size()!=EMA.numOfMachines)
				{}
				MessageTemplate mti = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
				ACLMessage rcvmsg = myAgent.receive(mti);
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setContent("M");
				if (rcvmsg!=null && rcvmsg.getContent().equals("START"))
				{
					char[] chars = new char[EMA.M];
					Arrays.fill(chars, '*');
					String content = new String(chars);
					msg.setContent(content);
					for (int i=0; i<MCAagents.size();i++)
					{
					     AID agentID = MCAagents.get(i).getAID();
					     //System.out.println("Spammer msg sent to " + agentID.getLocalName());
					     msg.addReceiver(agentID);

					     for(int j = 0 ; j < EMA.N ; j++)
					     {
					    	 send(msg);
					     } 
					}	
					myAgent.removeBehaviour(this);
				}	
				else
					block();
			}
		});
	}
	
	public static void registerMCA(MCA agent)
	{
		MCAagents.add(agent);
	}
	
}
