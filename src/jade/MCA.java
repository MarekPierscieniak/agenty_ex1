package jade;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class MCA extends Agent{
	//consume all msgs from SA (they know how many)
	//after receiving all send message DONE to EMA
	private int n=0;
	
	public void setup() {	
		SA.registerMCA(this);
		addBehaviour(new CyclicBehaviour(this)
		{
			public void action()
			{
				while(n<EMA.N*EMA.numOfMachines)
				{
				MessageTemplate mti = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
				ACLMessage rcvmsg = myAgent.receive(mti);
				if (rcvmsg!=null)
				{
					//System.out.println(myAgent.getLocalName()+ " received msg: " +rcvmsg.getContent());
					n++;
				}
				else
					block();
				}
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
				msg.setContent("DONE");
				msg.addReceiver(EMA.EMAID);
				send(msg);
				myAgent.removeBehaviour(this);
			}
		});
	}
}
	
